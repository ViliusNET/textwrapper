﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

/*==============================================================================================================
.NET TASK Vilius Turenko 2018-10-23
Create and implement algorithm that wraps text.Algorithm needs to have 2 arguments:
input text and the maximum number of symbols in one line.Algorithm has to split input text into lines in
such way that the length of each line is less or equal to the given maximum length.
Try to keep full word in one line. In other words, split word into
different lines only if the word length is greater than the given maximum line length.
Algorithm has to be covered with automated unit tests.
It would be great if the input text could be read from the file and output text saved to the file.
PS.Algorithm should handle multi line input text.

Example 1
Input text: “Green metal stick”
Max line length: 13
Expected output:
Green metal
stick
==============================================================================================================*/

namespace TextWrapper
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string dataPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TextFiles\");
            string resultPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"ResultTextFiles\");

            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }

            if (!Directory.Exists(resultPath))
            {
                Directory.CreateDirectory(resultPath);
            }
            else
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(resultPath);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }

            string[] filePaths = Directory.GetFiles(@dataPath, "*.txt");
            int i = 0;
            foreach (var file in filePaths)
            {
                i++;
                StreamReader reader = new StreamReader(file, Encoding.Default, true);
                var charAmount = reader.ReadLine();
                string textString = reader.ReadToEnd();
                var wrappedLines = TextWrapper.WrapText(textString, int.Parse(charAmount));
                File.AppendAllLines(@resultPath + $"results{i}.txt", wrappedLines);
            }

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("Great success");
            Console.WriteLine("Results are in ");
            Console.WriteLine(resultPath);
            Console.WriteLine("-------------------------------------------------------------------");
        }
    }
}