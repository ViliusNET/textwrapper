﻿using System;
using System.Collections.Generic;

namespace TextWrapper
{
    /// <summary>
    /// Text wrapper static class
    /// </summary>
    public static class TextWrapper
    {
        /// <summary>
        /// Main wrapping method
        /// </summary>
        /// <param name="text">Text that needs to be wrapped</param>
        /// <param name="charAmount">Maximum character amount in line</param>
        /// <returns></returns>
        public static IEnumerable<string> WrapText(string text, int charAmount)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentNullException("Empty text parameter");
            }

            if (charAmount <= 0)
            {
                throw new ArgumentNullException("Incorrect char amount, must be > 0");
            }

            #region Params

            int currentIndex = 0;
            var lastSplit = 0;
            var whitespaces = new[] { ' ', '\r', '\n', '\t' };
            var resultList = new List<string>();

            #endregion Params

            while (currentIndex < text.Length)
            {
                if (lastSplit + charAmount > text.Length)
                {
                    currentIndex = text.Length;
                }
                else
                {
                    currentIndex = (text.LastIndexOfAny(new[] { ' ', ',', '.', '?', '!', ':', ';', '-', '\n', '\r', '\t' }, Math.Min(text.Length - 1, lastSplit + charAmount)) + 1);
                }

                if (lastSplit >= currentIndex)
                {
                    currentIndex = Math.Min(lastSplit + charAmount, text.Length);
                }

                resultList.Add(text.Substring(lastSplit, currentIndex - lastSplit).Trim(whitespaces));
                lastSplit = currentIndex;
            }

            return resultList;
        }
    }
}