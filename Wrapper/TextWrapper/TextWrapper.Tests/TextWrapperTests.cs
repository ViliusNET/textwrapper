using System;
using System.Linq;
using Xunit;

namespace TextWrapper.Tests
{

    public class TextWrapperTests
    {
        [Fact]
        public void WrapText_TestLongWordsThatAreOverTheLimit()
        {
            var lines = TextWrapper.WrapText("Prisiki�kiakopustaliaujantiems", 9).ToArray();
            Assert.Equal(4, lines.Count());
            Assert.Equal("Prisiki�k", lines[0]);
            Assert.Equal("iakopusta", lines[1]);
            Assert.Equal("liaujanti", lines[2]);
            Assert.Equal("ems", lines[3]);
        }

        [Fact]
        public void WrapText_TestWordsThatAreInTheLimit()
        {
            var lines = TextWrapper.WrapText("Ki�kis i�b�go mi�kan", 6).ToArray();
            Assert.Equal(3, lines.Count());
            Assert.Equal("Ki�kis", lines[0]);
            Assert.Equal("i�b�go", lines[1]);
            Assert.Equal("mi�kan", lines[2]);
        }

        [Fact]
        public void WrapText_TestWordsThatAreBothInAndOverTheLimit()
        {
            var lines = TextWrapper.WrapText("Senai senai ki�kiakopustaliauti �jome visi kartu", 5).ToArray();
            Assert.Equal(9, lines.Count());
            Assert.Equal("Senai", lines[0]);
            Assert.Equal("senai", lines[1]);
            Assert.Equal("ki�ki", lines[2]);
            Assert.Equal("akopu", lines[3]);
            Assert.Equal("stali", lines[4]);
            Assert.Equal("auti", lines[5]);
            Assert.Equal("�jome", lines[6]);
            Assert.Equal("visi", lines[7]);
            Assert.Equal("kartu", lines[8]);
        }

        [Fact]
        public void WrapText_TestTextNotPassed()
        {
            try
            {
                TextWrapper.WrapText(null, 5);
            }
            catch (ArgumentException ex)
            {
                Assert.Equal("Empty text parameter", ex.ParamName);
            }
        }

        [Fact]
        public void WrapText_TestIncorrectCharAmount()
        {
            try
            {
                TextWrapper.WrapText("ki�kis", -5);
            }
            catch (ArgumentException ex)
            {
                Assert.Equal("Incorrect char amount, must be > 0", ex.ParamName);
            }
        }
    }
}